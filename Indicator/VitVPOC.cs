#region Using declarations

using System;
using System.ComponentModel;
using System.Drawing;
using NinjaTrader.Data;
using NinjaTrader.Gui.Chart;
using System.Collections.Generic;

#endregion

namespace NinjaTrader.Indicator {

	[Description("Vitalij's VPOC indicator for NinjaTrader 7" + "\r\n" + "http://ninjatrader.bitbucket.org/")]
	public class VitVPOC : Indicator {

		#region Variables

		int period = 1;
		Dictionary<double, double> dictPriceVolume = new Dictionary<double, double>();

		#endregion Variables

		#region Methods

		protected override void Initialize() {
			CalculateOnBarClose = true;
			Overlay = true;
			AutoScale = false;

			Name = "VPOC";

			Add(PeriodType.Tick, period);
			Add(new Plot(Color.Black, PlotStyle.Line, "vpoc"));
		}

		protected override void OnBarUpdate() {
			if (BarsInProgress == 1) {
				OnEachTick();
			} else if (dictPriceVolume.Count > 0) {
				OnEachBar();
			}
		}

		private void OnEachTick() {
			// throw away last session data
			if (Bars.FirstBarOfSession) {
				dictPriceVolume.Clear();
				PlotColors[0][-1] = Color.Transparent;
			}

			// sum up volume per price
			if (!dictPriceVolume.ContainsKey(Close[0])) {
				dictPriceVolume[Close[0]] = Volume[0];
			} else {
				dictPriceVolume[Close[0]] += Volume[0];
			}
		}

		private void OnEachBar() {
			// find price level with max. volume
			using (var enumerator = dictPriceVolume.GetEnumerator()) {
				KeyValuePair<double, double> max = enumerator.Current;
				while (enumerator.MoveNext()) {
					var kv = enumerator.Current;
					if (kv.Value > max.Value)
						max = kv;
				}

				Values[0].Set(max.Key);
			}
		}

		#endregion Methods

		#region Properties

		[Gui.Design.DisplayName("Tick period")]
		[Description("Bigger is faster but inaccurate. Volume of [x] ticks will be added to closing price level at once.")]
		public int Period {
			get { return period; }
			set { period = Math.Max(Math.Min(value, int.MaxValue), 1); }
		}

		#endregion Properties

	}
}

#region NinjaScript generated code. Neither change nor remove.
// This namespace holds all indicators and is required. Do not change it.
namespace NinjaTrader.Indicator {
	public partial class Indicator : IndicatorBase {
		private VitVPOC[] cacheVitVPOC = null;

		private static VitVPOC checkVitVPOC = new VitVPOC();

		/// <summary>
		/// VitVPOC
		/// </summary>
		/// <returns></returns>
		public VitVPOC VitVPOC() {
			return VitVPOC(Input);
		}

		/// <summary>
		/// VitVPOC
		/// </summary>
		/// <returns></returns>
		public VitVPOC VitVPOC(Data.IDataSeries input) {
			if (cacheVitVPOC != null)
				for (int idx = 0; idx < cacheVitVPOC.Length; idx++)
					if (cacheVitVPOC[idx].EqualsInput(input))
						return cacheVitVPOC[idx];

			lock (checkVitVPOC) {
				if (cacheVitVPOC != null)
					for (int idx = 0; idx < cacheVitVPOC.Length; idx++)
						if (cacheVitVPOC[idx].EqualsInput(input))
							return cacheVitVPOC[idx];

				VitVPOC indicator = new VitVPOC();
				indicator.BarsRequired = BarsRequired;
				indicator.CalculateOnBarClose = CalculateOnBarClose;
#if NT7
				indicator.ForceMaximumBarsLookBack256 = ForceMaximumBarsLookBack256;
				indicator.MaximumBarsLookBack = MaximumBarsLookBack;
#endif
				indicator.Input = input;
				Indicators.Add(indicator);
				indicator.SetUp();

				VitVPOC[] tmp = new VitVPOC[cacheVitVPOC == null ? 1 : cacheVitVPOC.Length + 1];
				if (cacheVitVPOC != null)
					cacheVitVPOC.CopyTo(tmp, 0);
				tmp[tmp.Length - 1] = indicator;
				cacheVitVPOC = tmp;
				return indicator;
			}
		}
	}
}

// This namespace holds all market analyzer column definitions and is required. Do not change it.
namespace NinjaTrader.MarketAnalyzer {
	public partial class Column : ColumnBase {
		/// <summary>
		/// VitVPOC
		/// </summary>
		/// <returns></returns>
		[Gui.Design.WizardCondition("Indicator")]
		public Indicator.VitVPOC VitVPOC() {
			return _indicator.VitVPOC(Input);
		}

		/// <summary>
		/// VitVPOC
		/// </summary>
		/// <returns></returns>
		public Indicator.VitVPOC VitVPOC(Data.IDataSeries input) {
			return _indicator.VitVPOC(input);
		}
	}
}

// This namespace holds all strategies and is required. Do not change it.
namespace NinjaTrader.Strategy {
	public partial class Strategy : StrategyBase {
		/// <summary>
		/// VitVPOC
		/// </summary>
		/// <returns></returns>
		[Gui.Design.WizardCondition("Indicator")]
		public Indicator.VitVPOC VitVPOC() {
			return _indicator.VitVPOC(Input);
		}

		/// <summary>
		/// VitVPOC
		/// </summary>
		/// <returns></returns>
		public Indicator.VitVPOC VitVPOC(Data.IDataSeries input) {
			if (InInitialize && input == null)
				throw new ArgumentException("You only can access an indicator with the default input/bar series from within the 'Initialize()' method");

			return _indicator.VitVPOC(input);
		}
	}
}
#endregion
